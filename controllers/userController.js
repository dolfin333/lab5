const path = require('path');
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository(path.resolve(__dirname, '../data/users.json'));

module.exports = {
    async getUserById(req, res) {
        try {
            const id = req.params.id;
            const user = await userRepository.getUserById(id);
            if (user) {
                if (user.biography == null) user.biography = 'No bio';
                res.status(200).send({
                    headTitle: 'User', usersCurrent: 'current',
                    user: user
                });
            }
            else {
                res.status(404).send({
                    headTitle: 'Users', usersCurrent: 'current',
                    user: null, msg: '404! No such user.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'User', usersCurrent: 'current',
                user: null, msg: 'Server error!'
            });
        }
    },

    async getUsers(req, res) {
        try {
            const strPage = req.query.page;
            const per_page = 4;
            let page;
            if (strPage === undefined) page = 1;
            else page = parseInt(strPage);
            if (page < 1 || isNaN(page)) page = 1;

            const users = await userRepository.getUsers();
            if (users) {
                const params = {
                    headTitle: 'Users', usersCurrent: 'current',
                    users: users
                };
                res.status(200).send(params);
            }
            else {
                res.status(200).send({
                    headTitle: 'Users', users: null,
                    usersCurrent: 'current'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'Users', usersCurrent: 'current',
                user: null, msg: 'Server error!'
            });
        }
    }
};