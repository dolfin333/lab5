const path = require('path');
const CourseRepository = require('../repositories/courseRepository')
const courseRepository = new CourseRepository(path.resolve(__dirname, '../data/courses.json'));

module.exports = {
    async getCourseById(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.getCoursesById(id);
            if (course) {
                res.status(200).render('course', {
                    headTitle: 'Courses', coursesCurrent:
                        'current', course: course
                });
            }
            else {
                res.status(404).render('error', {
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).render('error', {
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async getCourses(req, res) {
        res.status(200).render('courses', { headTitle: 'Courses', coursesCurrent: 'current' });
    },
};