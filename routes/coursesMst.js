const courseRouter = require('express').Router();
const courseController = require('../controllers/courseControllerMst.js');

courseRouter
    .get('/new', (req, res) => {
        res.status(200).render('newCourse', {
            headTitle: 'Create new Course', coursesCurrent: 'current'
        });
    })  
    /**
    * @route GET /api/courses/{id}
    * @group Courses - entity operations
    * @param {integer} id.path.required - id of the Course - eg: 1
    * @returns {Course.model} 200 - Course object
    * @returns {Error} 400 - Bad request
    * @returns {Error} 404 - Course was not found
    */
    .get('/:id',courseController.getCourseById)
     /**
    * @route GET /api/courses
    * @group Courses - entity operations
    * @param {integer} page.query - page number
    * @param {integer} per_page.query - items per page
    * @returns {Array.<Course>} Courses - all courses
    * @returns {Error} 404 - Courses were not found
    * @returns {Error} 400 - Bad request
    */
    .get('/', courseController.getCourses)

module.exports = courseRouter;