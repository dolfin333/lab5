const path = require('path');
const CourseRepository = require('../repositories/courseRepository')
const courseRepository = new CourseRepository(path.resolve(__dirname, '../data/courses.json'));
const MediaRepository = require(path.resolve(__dirname, '../repositories/mediaRepository'));
const mediaRepository = new MediaRepository(path.resolve(__dirname, '../data/media'));

const fs = require('fs');
const webConnections = require("../websocket/websocketArr").webConnArr;

function pagination(items, page, per_page) {
    const startInd = (page - 1) * per_page;
    const endInd = page * per_page;
    return (items.slice(startInd, endInd));
}

module.exports = {
    async getCourseById(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.getCoursesById(id);
            if (course) {
                res.status(200).send({
                    headTitle: 'Courses', coursesCurrent:
                        'current', course: course
                });
            }
            else {
                res.status(404).send({
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async getCourses(req, res) {
        try {
            let page;

            if (req.query.page && isNaN(req.query.page)) {
                res.status(400).render('error400', { mess: 'page and per_page should be a number' });
                return;
            }

            if (!req.query.page) {
                page = 1;
            } else {
                page = parseInt(req.query.page);
            }
            let per_page;
            if (!req.query.per_page)
                per_page = 4;
            else {
                per_page = 10;
            }

            let Courses;

            let name = "";
            if (req.query.name) {
                Courses = courseRepository.getCoursesByName(req.query.name);
                name = req.query.name;
            } else {
                Courses = courseRepository.getCourses();
            }
            const Pag_courses = pagination(Courses, page, per_page);

            let class_prev;
            let class_next;

            if (page > 1) {
                class_prev = "";
            } else {
                class_prev = "disabled_link";
            }

            if (page * per_page < Courses.length) {
                class_next = "";
            } else {
                class_next = "disabled_link";
            }

            let maxPage = Math.ceil(parseInt(Courses.length) / (per_page));
            if (maxPage == 0) {
                maxPage = 1;
            }
            const params = {
                headTitle: 'Courses', coursesCurrent: 'current',
                nameSearch: name, film_current: 'current', next_page: page + 1,
                previous_page: page - 1, page,
                class_prev, class_next, maxPage, courses: Pag_courses
            };

            res.status(200).send(params);
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },

    async addCourse(req, res) {
        try {
            if (Object.keys(req.files).length !== 0) {
                const fileFormat = req.files['course_img'].mimetype.split('/')[1];
                fs.writeFileSync(path.resolve(__dirname, '../data/media/' + mediaRepository.getNextId() +
                    '.' + fileFormat), req.files['course_img'].data, (err) => {
                        if (err) {
                            console.log("Cannot load image ", err);
                        }
                    })
                req.body.img = '/media/' + mediaRepository.getNextId() + '.' + fileFormat;
                mediaRepository.incrementId();
            }
            else req.body.img = null;
            const new_course = courseRepository.addCourse(req.body);
            for (const connection of webConnections) {
                connection.send(JSON.stringify({ course: new_course }))
            }
            res.status(303).redirect('/courses/' + new_course.id);
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    },
    async deleteCourseById(req, res) {
        try {
            const id = req.params.id;
            const course = await courseRepository.deleteCourse(id);
            if (course) {
                // res.status(303).redirect('/courses');
                res.status(200).send({
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: course
                });
            }
            else {
                res.status(404).send({
                    headTitle: 'Courses', coursesCurrent: 'current',
                    course: null, msg: '404! No such course.'
                });
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).send({
                headTitle: 'Courses', coursesCurrent: 'current',
                course: null, msg: 'Server error!'
            });
        }
    }
};