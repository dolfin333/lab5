const mstRouter = require('express').Router();
const userRouter = require('./usersMst.js');
const courseRouter = require('./coursesMst.js')

mstRouter.use('/users', userRouter);
mstRouter.use('/courses', courseRouter);

module.exports = mstRouter;